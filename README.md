# Duckietown Activity: Toy Car Controller

## Welcome to the 2024 ETH Duckietown Class!

This activity is designed as a self-assessment tool to help you gauge your comfort level with the necessary tools for an engaging and successful semester in the Duckietown course. Please submit in the questionnaire the result you have achieved and the parameter values you have used.

---

## Task Overview

### Objective

You will work with a simulated toy car and aim to achieve the best lap time possible on a designated track. To enhance the car's performance, you will be able to adjust the controller parameters, optimizing the tuning to reach faster speeds while ensuring the car stays on track.

---

## Accessing the Configuration File

The configuration file for the PID and Pure Pursuit controllers can be found here:
[src/CONTROLLER_PARAMS.py](src/CONTROLLER_PARAMS.py)

It should look like this:

![Configuration File Screenshot](videos/screenshot_params_file.png)

Feel free to try different values for these parameters, to get to higher target speeds while maintaining stability.

### Understanding the Controllers

- **PID Controller**: A Proportional-Integral-Derivative (PID) controller uses three terms to adjust the control output: 
  - **Proportional**: Reacts to current error.
  - **Integral**: Reacts to the accumulation of past errors.
  - **Derivative**: Reacts to the rate of change of the error.

**How it works**: The PID controller aims here to minimize the error between the desired and actual speed by adjusting the throttle based on these three terms. Tuning the PID constants is key to balancing fast response times with stability. For instance, a high proportional gain might cause overshooting, while too much integral action may lead to instability over time.

For more information on the PID controller, and a small guide on tuning methods, you can check the following link :
https://medium.com/autonomous-robotics/pid-control-85596db59f35

- **Pure Pursuit Controller**: This controller follows a path by calculating the steering angle needed to reach a target point on the path ahead of the car, allowing for smoother turns and better trajectory tracking.

**How it works**: The Pure Pursuit controller looks ahead to a designated "look-ahead point" on the path and computes the steering angle needed to reach it. This look-ahead distance influences how aggressive or smooth the steering will be; a shorter distance results in sharper turns, while a longer distance yields smoother, but slower, turns.




For more information on the pure pursuit controller and a proper derivation of the equations, you can check out the following link:
https://dingyan89.medium.com/three-methods-of-vehicle-lateral-control-pure-pursuit-stanley-and-mpc-db8cc1d32081

---

## Getting Started

### Prerequisites

Before running the code, ensure you do the following:

1. **Clone the Repository**
   ```bash
   git clone https://gitlab.ethz.ch/duckietown_eth_2024/duckietown-introduction-toy-car-controller.git

   cd duckietown-introduction-toy-car-controller


2. **Install Requirements** Install the necessary packages from requirements.txt. You may want to verify compatibility based on your environment, or use a virtual environment with conda for example

    ``` bash
    pip install -r requirements.txt


3. **Edit Controller Parameters** Modify the controller parameters in:

    [src/CONTROLLER_PARAMS.py](src/CONTROLLER_PARAMS.py)

    We recommend you install and use a code editor, for example Visual Studio Code.

4. **Run the Simulation** Execute the simulation to visualize the controller's performance:

    ```bash
    python3 src/run.py


**TIP**: If the simulation is too laggy, we recommend you increase the *sim_dt* and the *render_dt* variables in the [src/run.py](src/run.py) file to accomodate with your laptop's capabilities at best. However it is important to keep the *sim_dt* variable quite low to avoid inconsistencies in the integration of the dynamics. Therefore we recommend starting with *render_dt* and keeping *sim_dt* lower than *render_dt* at all times. Don't worry about this affecting the laptimes, as they are computed solely based on the dynamics and do not rely on the simulation step times, or on an internal timer.

## Example of the controller performance

### Too fast - collision

![Controller Performance Collision](videos/collision.gif)

Here the target speed is a little too ambitious and results in the car not being able to take all the corners. Some extra tuning might be required to make it at this speed, but lowering the target might be the only way.

### Slower and smooth - No collision

![Controller Performance No Collision](videos/no-collision.gif)

Here with a lower target speed, the car is able to finish the lap without cheating and leaving the track. It is now time to try a little faster !


## Submit

Please submit in the questionnaire the best lap time you have achieved and the parameter values you have used.

